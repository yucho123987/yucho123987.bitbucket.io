$("#more").click(function(){
  $("#menu").toggle()
});
$("#show-about-message").click(function(){
  alert("新闻数据来源于知乎《每天60秒读懂世界》专栏，可能会出现更新不及时、内容错误、错别字等情况，望周知。")
});
$("#refresh-page").click(function(){
  window.location.reload()
});
$.get("https://api.yucho258.tk/news.php",function(result){
  var data=JSON.parse(result);
  if (data.successful==true) {
    var updateTimeObject=new Date(data.updateTime*1000);
    var updateMonth=updateTimeObject.getMonth()+1;
    var updateHour=updateTimeObject.getHours();
    var updateMinute=updateTimeObject.getMinutes();
    var updateSecond=updateTimeObject.getSeconds();
    if (updateHour<10) {
      var updateHour="0"+updateHour
    }if (updateMinute<10) {
      var updateMinute="0"+updateMinute
    }if (updateSecond<10) {
      var updateSecond="0"+updateSecond
    }
    var updateTime=updateTimeObject.getFullYear()+" 年 "+updateMonth+" 月 "+updateTimeObject.getDate()+` 日 ${updateHour}:${updateMinute}:${updateSecond}`;
    $("#main").html(`<div>更新于 ${updateTime}</div><div>${data.message}</div>`);
    $("#main img").attr("width","100%")
  }else{
    $("#main").html(`加载失败，因为：${data.message}`)
  }
})